//object that handles client request and send responses
//property of express module
	// Router();

const express = require("express");
const router = express.Router();

//in order to use controllers module properties, import controllers module
const userController = require("./../controllers/userControllers");


//retrieving array of documents from database using "GET" method and find() model method
router.get("/", (req, res) => {
	//check the request if there's data to be used
	// console.log(req);

	//invoke the function from controllers
	userController.getAllUsers().then ( result => {
		res.send (result)
	})
})

//Add a user in the database using "POST" method and save() method
router.post("/add-user", (req,res) => {

	//check the request if there's data to be used
	console.log(req.body);

	//invoke the function from controllers
	userController.register(req.body).then ( (result) => res.send (result))
})

router.put ("/update-user", (req,res) => {
	console.log (req.body);

	userController.updateUser(req.body.email).then (result => res.send (result))

})

router.delete ("/delete-user" , (req,res) => {

	console.log (req.body);

	userController.deleteUser(req.body.email).then (result => res.send (result))

})

/* Retrieve a specific user using two model methods
findOne()
findById()

*/

router.get("/specific-user", (req,res) => {
	console.log (req.body);

	userController.getSpecificUser(req.body.email).then (result => res.send(result))
})

//6194f751ad366aff4fa940ca
router.get ("/:id", (req,res) => {

	//request
		//params ---> id
		//body
		//http methods
		//headers
	

	userController.getById(req.params.id).then (result => res.send(result))
})


//in order for the routes to be used in other modules we need to export it first
module.exports = router;