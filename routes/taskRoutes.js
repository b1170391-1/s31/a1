const express = require ("express");
const router = express.Router();

const taskControllers = require ("./../controllers/taskControllers")

router.post ("/", (req,res) => {
	taskControllers.createTask(req.body).then (result => res.send (result));
})

router.get("/getAllTask", (req, res) => {
	taskControllers.getAllTask(req.body).then(result => res.send(result));
})

router.get("/:id", (req,res) => {
	taskControllers.getSpecificTaskbyId(req.params.id).then (result => res.send(result));
})

router.put ("/change-status", (req,res) => {
	taskControllers.changeStatus(req.body.status).then (result => res.send(result));
})




module.exports = router;