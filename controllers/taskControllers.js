
const Task = require ('./../models/Task')

module.exports.createTask = (reqBody) => {
	let newTask = new Task ({
		name: reqBody.name
	})

	return newTask.save().then (result => result)
}

module.exports.updateTask = (reqBody) => {
	let updateTask = new Task ({
		name: reqBody.id
	})

	return updateTask.save().then (result => result)
}

module.exports.getAllTask = (reqBody) => {
	 return Task.find({}).then(result => result);
}

module.exports.getSpecificTaskbyId = (params) => {
	return Task.findById(params).then (result => result)
}

module.exports.changeStatus = (status) => {

	console.log (status);

	let updateStatus = {status: "completed"}

	return Task.findOneAndUpdate ({status:status}, updateStatus, {new:"completed"}).then (result => result)

}